---
- name: Send Windows Updates report
  hosts: all
  gather_facts: true
  vars:
    sendgrid_api_key: "{{ lookup('env','SENGRID_API_KEY') }}"
    slack_token: "{{ lookup('env','SLACK_TOKEN') }}"
    slack_channel: "{{ lookup('env','SLACK_CHANNEL') }}"
    send_email_sendgrid: "{{ send_email_sendgrid }}"
    sendgrid_from_email_address: "cfernand@redhat.com"
    sendgrid_to_email_addresses: "{{ sendgrid_to_email_addresses }}"
    send_slack_notification: "{{ send_slack_notification }}"
    save_report_locally: "{{ save_report_locally }}"
    win_update_category_names: "{{ win_update_category_names }}" 

  tasks:
     - name: Check for available Windows updates
       ansible.windows.win_updates:
         category_names: "{{ win_update_category_names }}"
         state: searched
       register: available_updates
     
     - name: Send email via Sendgrid
       community.general.sendgrid:
         api_key: "{{ sendgrid_api_key | default(omit) }}"
         from_address: "{{ sendgrid_from_email_address }}" 
         to_addresses: "{{ sendgrid_to_email_addresses }}"
         subject: "[Ansible] Windows Update report"
         body: "{{ lookup('template', '../templates/update_report.html.j2') }}"
         html_body: yes
       delegate_to: localhost
       run_once: True
       when: send_email_sendgrid | bool
     
     - name: Send notification message via Slack
       block:
         - name: Send slack notification for Non-Compliant Windows servers
           community.general.slack:
             token: "{{ slack_token | default(omit) }}"
             msg: |
                 ### Windows Update Status ###
                 --------------------------------------
                 `Server`: {{ ansible_hostname }}
                 `Status`: Non Complaint.
                 `Pending Updates`: {{ available_updates.found_update_count }}
                 --------------------------------------
             channel: "{{ slack_channel }}"
             color: danger
             username: "Ansible on {{ inventory_hostname }}"
             link_names: 0
             parse: 'none'
           delegate_to: localhost
           when: available_updates.found_update_count > 0
     
         - name: Send slack notification for Compliant Windows servers
           community.general.slack:
             token: "{{ slack_token | default(omit) }}"
             msg: |
                 ### Windows Update Status ###
                 --------------------------------------
                 `Server`: {{ ansible_hostname }}
                 `Status`: Complaint.
                 `Pending Updates`: {{ available_updates.found_update_count }}
                 --------------------------------------
             channel: "{{ slack_channel }}"
             color: good
             username: "Ansible on {{ inventory_hostname }}"
             link_names: 0
             parse: 'none'
           delegate_to: localhost
           when: available_updates.found_update_count == 0
       when: send_slack_notification | bool
     
     - name: Save report locally
       ansible.builtin.template:
         src: ../templates/update_report.html.j2
         dest: update_report.html
       delegate_to: localhost
       become: no
       when: save_report_locally | bool
