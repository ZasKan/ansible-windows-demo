# ansible-windows-demo

# demo-environment

Ansible Windows Demo

## Demo/Lab Developers:

*Cesar Fernandez*, EMEA Ansible Specialist Solution Architect, Red Hat

## Prerequisites

- Twilio SendGrid account.
- Azure account.
- Ansible Automation Controller.
- Ansible Navigator.
- Demo Environment project.

### Twilio SendGrid account

You can request a Twilio SendGrid account in the following [link](https://sendgrid.com/).

### Azure account

If you don't have an Azure account, you can request one in the following [link](https://demo.redhat.com/catalog?category=Open_Environments&item=babylon-catalog-prod%2Fazure-gpte.open-environment-azure.prod).

### Ansible Automation Controller

If you don't have an Automation Controller, you can get your product trial in the following link [link](https://www.redhat.com/en/technologies/management/ansible/trial?extIdCarryOver=true&sc_cid=701f2000001OH7YAAW).

### Ansible Navigator

- If you have Linux, you can install the `ansible-navigator` tool following the steps described in the following [link](https://ansible-navigator.readthedocs.io/installation/#linux).

> Note: Run the command `ansible-navigator --version` to make sure it was installed correctly.

## Demo Environment Project

- Clone the demo environment project:

```sh
cd ~
git clone add origin https://gitlab.com/cfernand/ansible-windows-demo.git
```

## Demo Environment Deployment

- Deploy the demo environment:

```sh
cd ~/ansible-windows-demo/ansible-navigator/
ansible-navigator run ../aap-iac/ansible-demo-deploy.yml -m stdout \
  -e 'ansible_python_interpreter=/usr/bin/python3' \
  -e 'aap_host=<AAP_CTL_HOST>' \
  -e 'aap_user=<AAP_CTL_USER>' \
  -e 'aap_pass=<AAP_CTL_PASSWORD>' \
  -e 'azure_subscription_id=<AZURE_SUBSCRIPTION_ID>' \
  -e 'azure_client_id=<AZURE_CLIENT_ID>' \
  -e 'azure_client_secret=<AZURE_CLIENT_SECRET>' \
  -e 'azure_tenant_id=<AZURE_TENANT_ID>' \
  -e 'azure_resource_group=<AZURE_RESOURCE_GROUP>' \
  -e 'windows_password=<WINDOWS_PASSWORD>*'
  
[*]
- Password must be eight or more characters long.
- Password must contain characters from two of the following four categories:
    - Uppercase characters A-Z (Latin alphabet)
    - Lowercase characters a-z (Latin alphabet)
    - Digits 0-9
    - Special characters (!, $, #, %, etc.)
```
